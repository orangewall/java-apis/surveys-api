package com.orangewall.surveys.common.authentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Method;
import java.util.*;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;


/**
 * This filter verify the access permissions for a user
 * based on username and passowrd provided in request
 * */
@Provider
@Priority(Priorities.AUTHENTICATION)
@Component
public class AuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationFilter.class);

    @Context
    private ResourceInfo resourceInfo;

    @Value("${authentication.header-property:Authorization}")
    private String authorizationHeader;

    @Value("${authentication.scheme:Bearer}")
    private String authenticationScheme;

    @Value("${authentication.url}")
    private String authenticationUrl;

    @Context
    private transient HttpServletRequest servletRequest;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        if (requestContext.getRequest().getMethod().equals("OPTIONS")) {
            return;
        }

        RequestUser requestUser = getRequestUser(requestContext);

        if (requestUser != null) {
            servletRequest.setAttribute(AuthenticationConsts.REQUEST_USER_ID, requestUser);
        }

        Method method = resourceInfo.getResourceMethod();
        //Access allowed for all
        if (!method.isAnnotationPresent(PermitAll.class)) {
            //Access denied for all
            if (method.isAnnotationPresent(DenyAll.class)) {
                Response accessForbiden = Response.status(Response.Status.FORBIDDEN).entity("Access blocked for all users !!").build();
                requestContext.abortWith(accessForbiden);
                LOGGER.warn("Access Forbidden because resource has 'DenyAll' annotation. RequestUrl: '{}'", requestContext.getUriInfo().getAbsolutePath());
                return;
            }

            //If no authorization information present; block access
            if (requestUser == null) {
                Response accessDenied = Response.status(Response.Status.UNAUTHORIZED).entity("You cannot access this resource").build();
                requestContext.abortWith(accessDenied);
                LOGGER.warn("Unauthorized user because resource does not have 'PermitAll' annotation and no valid token was provided. RequestUrl: '{}'", requestContext.getUriInfo().getAbsolutePath());
                return;
            }

            //Verify user access
            if (method.isAnnotationPresent(RolesAllowed.class)) {
                RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
                Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));

                //Is user valid?
                if (!isUserAllowed(requestUser, rolesSet)) {
                    Response accessDenied = Response.status(Response.Status.UNAUTHORIZED).entity("You cannot access this resource").build();
                    requestContext.abortWith(accessDenied);
                    LOGGER.warn("Unauthorized user because does not have the right role. User Role: '{}'; Allowed Roles: '{}';  RequestUrl: '{}'", requestUser.getRole(), rolesSet, requestContext.getUriInfo().getAbsolutePath());
                    return;
                }
            }
        }
    }

    private boolean isUserAllowed(RequestUser requestUser, Set<String> rolesSet) {
        if (rolesSet.contains("*")) {
            return true;
        }
        if (requestUser.getRole() != null && rolesSet.contains(requestUser.getRole())) {
            return true;
        }
        return false;
    }

    private RequestUser getRequestUser(ContainerRequestContext requestContext) {
        //Get request headers
        final MultivaluedMap<String, String> headers = requestContext.getHeaders();

        //Fetch authorization header
        final List<String> authorization = headers.get(authorizationHeader);

        if (authorization == null || authorization.isEmpty()) {
            return null;
        }

        //Get encoded token
        final String token = authorization.get(0).replaceFirst(authenticationScheme + " ", "");

        if (token == null || token.isEmpty()) {
            return null;
        } else {
            RestTemplate restTemplate = new RestTemplate();
            RequestUser requestUser = restTemplate.postForObject(this.authenticationUrl, token, RequestUser.class, new Object[0]);
            return requestUser;
        }
    }
}