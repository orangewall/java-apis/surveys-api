package com.orangewall.surveys.common.authentication;

public enum  UserRole {
    admin,
    reviewer
}
