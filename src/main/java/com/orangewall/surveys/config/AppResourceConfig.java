package com.orangewall.surveys.config;

import com.orangewall.surveys.common.CORSFilter;
import com.orangewall.surveys.common.authentication.AuthenticationFilter;
import com.orangewall.surveys.resources.authentication.AuthenticationResource;
import com.orangewall.surveys.resources.logging.LoggingResource;
import com.orangewall.surveys.resources.survey.SurveyResource;
import com.orangewall.surveys.resources.surveyInstance.AssignSurveyInstanceResource;
import com.orangewall.surveys.resources.surveyInstance.SurveyInstanceResource;
import com.orangewall.surveys.resources.user.UserResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 * Created by Arjan on 11/26/2017.
 */
@Component
public class AppResourceConfig extends ResourceConfig {
    public AppResourceConfig() {
        //packages("com.orangewall.surveys.resources"); //This doesnt work with Spring 1.4
        register(SurveyInstanceResource.class);
        register(AuthenticationResource.class);
        register(SurveyResource.class);
        register(AssignSurveyInstanceResource.class);
        register(LoggingResource.class);
        register(UserResource.class);

        register(CORSFilter.class);
        register(AuthenticationFilter.class);
    }
}
