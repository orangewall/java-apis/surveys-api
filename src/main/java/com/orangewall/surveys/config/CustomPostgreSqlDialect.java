package com.orangewall.surveys.config;

import org.hibernate.dialect.PostgreSQL94Dialect;

import java.sql.Types;

import java.sql.Types;

public class CustomPostgreSqlDialect extends PostgreSQL94Dialect {

    public CustomPostgreSqlDialect() {
        super();
        this.registerColumnType(Types.JAVA_OBJECT, "jsonb");
    }
}