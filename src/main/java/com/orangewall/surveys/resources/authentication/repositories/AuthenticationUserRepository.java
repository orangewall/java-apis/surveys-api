package com.orangewall.surveys.resources.authentication.repositories;

import com.orangewall.surveys.resources.authentication.model.AuthenticationUser;
import org.springframework.data.repository.CrudRepository;

public interface AuthenticationUserRepository extends CrudRepository<AuthenticationUser, Long> {

    AuthenticationUser getByUsername(String username);

}