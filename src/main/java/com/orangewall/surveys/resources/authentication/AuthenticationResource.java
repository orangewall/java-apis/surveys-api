package com.orangewall.surveys.resources.authentication;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.orangewall.surveys.common.authentication.UserRole;
import com.orangewall.surveys.resources.authentication.model.AuthenticationUser;
import com.orangewall.surveys.resources.authentication.model.Credentials;
import com.orangewall.surveys.common.authentication.RequestUser;
import com.orangewall.surveys.resources.authentication.repositories.GeneratedTokenRepository;
import com.orangewall.surveys.resources.user.PasswordUtil;
import com.orangewall.surveys.resources.authentication.repositories.AuthenticationUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by Marseld on 6/5/17.
 */
@Path("authentication")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class AuthenticationResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationResource.class);

    @Value("${authentication.token.timeout:3600}")
    private int authenticationTokenTimeout;

    @Value("${authentication.token.encryption.key}")
    private String authenticationTokenEncryptionKey;

    @Autowired
    AuthenticationUserRepository userRepository;

    @Autowired
    GeneratedTokenRepository generatedTokenRepository;


    @Context //This annotation is used to inject objects from context. Objects like UserInfo, Request etc
    private UriInfo uriInfo;

    @Path("/authenticate")
    @POST
    @PermitAll
    @Produces(MediaType.TEXT_PLAIN)
    public Response authenticateJwt(Credentials credentials) {
        try {
            AuthenticationUser user = userRepository.getByUsername(credentials.getUsername());

            if (user == null || !user.isActive() || !PasswordUtil.check(credentials.getPassword(), user.getPassword())) {
                //TODO: Check if authentication should return UNAUTHORIZED or just return code 200 with body as null.
                return Response.status(Response.Status.UNAUTHORIZED).build();
            } else {
                //TODO: Check which Algorithm is the best Option. This should be good as well. https://github.com/auth0/java-jwt
                Algorithm algorithm = Algorithm.HMAC256(authenticationTokenEncryptionKey);

                LocalDateTime ldt = LocalDateTime.now().plusHours(1);
                Date expirationDate = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());

                String token = JWT.create()
                        .withExpiresAt(expirationDate)
                        .withClaim("role", user.getRole())
                        .withClaim("userId", user.getId())
                        .withIssuer("ow")
                        .sign(algorithm);


                LOGGER.info("User: '{}' authenticated successfully!", user.getUsername());
                return Response.ok(token).build();
            }
        } catch (IllegalStateException e) {
            LOGGER.error("Unexpected error happened on authenticating the user!", e);
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Error when encoding the token!", e);
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }


    @Path("/validate-token")
    @POST
    @PermitAll
    public Response validateJwt(String token) {
        try {
            RequestUser requestUser = new RequestUser();
            Algorithm algorithm = Algorithm.HMAC256(authenticationTokenEncryptionKey);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("ow")
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);
            Claim roleClaim = jwt.getClaim("role");
            if (roleClaim != null) {
                requestUser.setRole(Enum.valueOf(UserRole.class, roleClaim.asString()));
            }
            Claim userClaim = jwt.getClaim("userId");
            if (userClaim != null) {
                requestUser.setUserId(userClaim.asLong());
            }
            requestUser.setExpiresAt(jwt.getExpiresAt());
            return Response.ok(requestUser).build();
        } catch (UnsupportedEncodingException ex) {
            //UTF-8 encoding not supported
            LOGGER.error("Error when verifying the token!", ex);
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } catch (TokenExpiredException ex) {
            LOGGER.info(ex.getMessage());
            return Response.ok(null).build();
        } catch (JWTVerificationException ex) {
            //Invalid signature/claims
            LOGGER.error("Token is not valid!", ex);
            return Response.ok(null).build();
        }
    }

}
