package com.orangewall.surveys.resources.authentication.repositories;


import com.orangewall.surveys.resources.authentication.model.GeneratedToken;
import org.springframework.data.repository.CrudRepository;

public interface GeneratedTokenRepository extends CrudRepository<GeneratedToken, String> {

}
