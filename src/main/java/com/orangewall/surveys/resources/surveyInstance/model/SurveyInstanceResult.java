package com.orangewall.surveys.resources.surveyInstance.model;

import java.util.HashMap;

/**
 * Created by Arjan on 6/24/2018.
 */
public class SurveyInstanceResult {
    private Long id;
    private String accountId;
    private String callId;
    private Long surveyId; //?????
    private HashMap<String, String> answers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Long getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }

    public HashMap<String, String> getAnswers() {
        return answers;
    }

    public void setAnswers(HashMap<String, String> answers) {
        this.answers = answers;
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }
}
