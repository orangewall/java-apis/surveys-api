package com.orangewall.surveys.resources.surveyInstance;

import com.orangewall.surveys.common.authentication.AuthenticationConsts;
import com.orangewall.surveys.common.authentication.RequestUser;
import com.orangewall.surveys.resources.surveyInstance.model.SurveyInstance;
//import com.orangewall.surveys.resources.surveyInstance.util.SqsQueueSender;
import com.orangewall.surveys.resources.surveyInstance.model.SurveyInstanceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.NotificationMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.time.LocalDateTime;


/**
 * Created by Arjan on 11/10/2017.
 */

@Api
@Path("surveys/{surveyId}/surveyInstances")
@Component
public class SurveyInstanceResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(SurveyInstanceResource.class);

    @Autowired
    SurveyInstanceRepository surveyInstanceRepository;

    @Autowired
    private NotificationMessagingTemplate notificationMessagingTemplate;

    @Value("${survey-ms.results.topic}")
    String resultsTopic;

    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    @Context
    HttpServletRequest requestContext;

    RequestUser getRequestUser() {
        Object obj = requestContext.getAttribute(AuthenticationConsts.REQUEST_USER_ID);
        return (RequestUser) obj;
    }


    @ApiOperation(value = "Get Survey Instance by Id",
            notes = "You need to provide the id",
            response = SurveyInstance.class
    )
    @GET
    @RolesAllowed("*")
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getSurveyInstanceById(@PathParam("id") Long id) {

        try {
            SurveyInstance surveyInstanceToReturn = surveyInstanceRepository.findOne(id);
            if (surveyInstanceToReturn == null) {
                LOGGER.warn("Couln't find a SurveyInstance with id: " + id);
                return Response.status(Response.Status.NOT_FOUND).build();
            } else {
                LOGGER.info("SurveyInstance with id '{}' found successfully!", id);
                return Response.ok(surveyInstanceToReturn).build();
            }
        } catch (Exception ex) {
            LOGGER.error("Unexpected error when finding SurveyInstance with id: " + id, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }



    @RolesAllowed("*")
    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response postSurveyAnswer(@PathParam("surveyId") Long surveyId, SurveyInstance surveyInstance) {

        try {
            surveyInstance.setSurveyId(surveyId);
            validateCreateSurveyInstance(surveyInstance);
            surveyInstance.setAccountId("AC-12345");
            surveyInstance.setCreatedOn(LocalDateTime.now());
            surveyInstance.setReviewerId(getRequestUser().getUserId()); //Implement logic for reviewerId
            surveyInstanceRepository.save(surveyInstance);
        } catch (IllegalArgumentException ex) {
            LOGGER.error("surveyInstance is not valid! Error: {}, surveyInstance: {}", ex.getMessage(), surveyInstance);
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (Exception ex) {
            LOGGER.error("Unexpected error when saving SurveyInstance:  " + surveyInstance, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        URI location = uriInfo.getAbsolutePathBuilder()
                .path("{id}")
                .resolveTemplate("id", surveyInstance.getId())
                .build();

        LOGGER.info("SurveyInstance with id '{}' created successfully!", surveyInstance.getId());
        return Response.created(location).build();
    }

    @RolesAllowed("test")
    @Path("/{id}")
    @PUT
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response putSurveyAnswer(@PathParam("surveyId") Long surveyId, @PathParam("id") Long instanceId, SurveyInstance newSurveyInstance) {
        SurveyInstance surveyInstance = null;
        try {
            //TODO: Add a proper error logging that indicates which part is failing. we will need to create separate reports or alerts if none of them happened or just posting to the queue failed.
            validateUpdateSurveyInstance(newSurveyInstance);
            surveyInstance = surveyInstanceRepository.findOne(instanceId);
            Assert.notNull(surveyInstance, "Can not find a surveyInstance in database with provided id");
            surveyInstance.setReviewerId(getRequestUser().getUserId());
            surveyInstance.setAnswers(newSurveyInstance.getAnswers());
            surveyInstanceRepository.save(surveyInstance);

            SurveyInstanceResult result = new SurveyInstanceResult();
            result.setAccountId(surveyInstance.getAccountId());
            result.setId(surveyInstance.getId());
            result.setAnswers(surveyInstance.getAnswers());
            result.setCallId(surveyInstance.getCallId());
            result.setSurveyId(surveyInstance.getSurveyId());
            notificationMessagingTemplate.convertAndSend(resultsTopic, result);

        } catch (IllegalArgumentException ex) {
            LOGGER.error("surveyInstance is not valid! Error: {}, surveyId: '{}', surveyInstance: {}", ex.getMessage(), instanceId, newSurveyInstance);
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (Exception ex) {
            LOGGER.error("Unexpected error when saving SurveyInstance:  " + newSurveyInstance, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

//        URI location = uriInfo.getAbsolutePathBuilder()
//                .path("{id}")
//                .resolveTemplate("id", instanceId)
//                .build();

        LOGGER.debug("SurveyInstance with id '{}' created successfully! Existing SurveyInstance in DB: '{}', New SurveyInstance: '{}", instanceId, surveyInstance, newSurveyInstance);
        LOGGER.info("SurveyInstance with id '{}' created successfully!", instanceId);
        return Response.ok().build();
    }

    @GET
    @PermitAll
    @Produces({MediaType.TEXT_PLAIN})
    @Path("/health")
    public String checkHeath() {
        return "healthy!";
    }


    /**
     * Validates posted survey instance and make sure the data is valid
     * @param surveyInstance
     * @throws IllegalArgumentException
     */
    private void validateCreateSurveyInstance(SurveyInstance surveyInstance) throws IllegalArgumentException {
        Assert.notNull(surveyInstance, "surveyInstance must not be null");
        Assert.notNull(surveyInstance.getSurveyId(), "surveyInstance can not have a surveyId = null!");
        Assert.notNull(surveyInstance.getCallId(), "surveyInstance can not have a callId = null!");
    }


    /**
     * Validates posted survey instance and make sure the data is valid
     * @param surveyInstance
     * @throws IllegalArgumentException
     */
    private void validateUpdateSurveyInstance(SurveyInstance surveyInstance) throws IllegalArgumentException {
        Assert.notNull(surveyInstance, "surveyInstance must not be null");
        Assert.notNull(surveyInstance.getSurveyId(), "surveyInstance can not have a surveyId = null!");
        Assert.notNull(surveyInstance.getAnswers(), "surveyInstance can not have a answers = null!");
        Assert.notEmpty(surveyInstance.getAnswers(), "surveyInstance has zero answers!");
//        Assert.notNull(surveyInstance.getReviewerId(), "surveyInstance can not have a reviewerId = null!");
        Assert.notNull(surveyInstance.getCallId(), "surveyInstance can not have a callId = null!");
    }


}
