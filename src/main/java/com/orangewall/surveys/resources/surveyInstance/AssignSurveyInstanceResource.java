package com.orangewall.surveys.resources.surveyInstance;

import com.orangewall.surveys.common.authentication.AuthenticationConsts;
import com.orangewall.surveys.common.authentication.RequestUser;
import com.orangewall.surveys.resources.surveyInstance.model.SurveyInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.time.LocalDateTime;


@Path("surveyInstances")
@Component
public class AssignSurveyInstanceResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(AssignSurveyInstanceResource.class);

    private Integer claimerId = 3;

    @Autowired
    SurveyInstanceRepository surveyInstanceRepository;


    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    @Context
    HttpServletRequest requestContext;

    RequestUser getRequestUser() {
        Object obj = requestContext.getAttribute(AuthenticationConsts.REQUEST_USER_ID);
        return (RequestUser) obj;
    }



    @PermitAll
    @POST
    @Path("/assignSurvey")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response assignSurveyInstance() {
        try {
            SurveyInstance surveyInstance = surveyInstanceRepository.getTop1SurveyInstanceIdToAssign();
            if(surveyInstance == null) {
                return null;
            } else {
                surveyInstance.setClaimedOn(LocalDateTime.now());
                surveyInstance.setClaimerId(getRequestUser().getUserId());
                surveyInstanceRepository.save(surveyInstance);
                return Response.ok(surveyInstance).build();
            }
        } catch (Exception ex) {
            LOGGER.error("Unexpected error when assigning SurveyInstance to user" , ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GET
    @PermitAll
    @Produces({MediaType.TEXT_PLAIN})
    @Path("/health")
    public String checkHeath() {
        return "healthy!";
    }

}
