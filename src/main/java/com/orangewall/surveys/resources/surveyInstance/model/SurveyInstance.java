package com.orangewall.surveys.resources.surveyInstance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orangewall.surveys.resources.surveyInstance.model.types.HstoreUserType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;
import java.util.HashMap;

/**
 * Created by Arjan on 11/18/2017.
 */
@XmlRootElement
@Entity
@Table(name = "survey_instances")
@TypeDef(name = "hstore", typeClass = HstoreUserType.class)
public class SurveyInstance {

    @Id
    @GeneratedValue
    private Long id;
    private String accountId;
    private Long surveyId;
    @Type(type = "hstore")
    @Column(columnDefinition = "hstore", nullable = true) //hstore is the key:value pair in postgress. You can even use indexes.
    private HashMap<String, String> answers;
    private Long reviewerId;
    @JsonIgnore
    private Long claimerId;
    @JsonIgnore
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime claimedOn;
    private String callId;
    @JsonIgnore
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime createdOn;
    private String audioFileLocation;


    public Long getClaimerId() {
        return claimerId;
    }

    public String getAudioFileLocation() {
        return audioFileLocation;
    }

    public void setAudioFileLocation(String audioFileLocation) {
        this.audioFileLocation = audioFileLocation;
    }

    public void setClaimerId(Long claimerId) {
        this.claimerId = claimerId;
    }

    public LocalDateTime getClaimedOn() {
        return claimedOn;
    }

    public void setClaimedOn(LocalDateTime claimedOn) {
        this.claimedOn = claimedOn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Long getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }

    public HashMap<String, String> getAnswers() {
        return answers;
    }

    public void setAnswers(HashMap<String, String> answers) {
        this.answers = answers;
    }

    public Long getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(Long reviewerId) {
        this.reviewerId = reviewerId;
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "SurveyInstance{" +
                "id=" + id +
                ", accountId='" + accountId + '\'' +
                ", surveyId=" + surveyId +
                ", answers=" + answers +
                ", reviewerId=" + reviewerId +
                ", callId='" + callId + '\'' +
                ", createdOn=" + createdOn +
                '}';
    }
}
