package com.orangewall.surveys.resources.surveyInstance;

import com.orangewall.surveys.resources.surveyInstance.model.SurveyInstance;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Arjan on 11/26/2017.
 */
public interface SurveyInstanceRepository extends PagingAndSortingRepository<SurveyInstance, Long>{
//    List<SurveyInstance> findByReviewerId(Integer reviewerId);
//    List<SurveyInstance> findByCallId(Integer callId);
//    SurveyInstance findBySurveyIdAndCallId(Integer surveyId, Integer callId);

    @Query(value = "SELECT * FROM survey_instances si WHERE si.reviewer_id IS NULL AND claimer_id IS NULL ORDER BY si.created_on LIMIT 1", nativeQuery = true)
    SurveyInstance getTop1SurveyInstanceIdToAssign();

}
