package com.orangewall.surveys.resources.survey;

import com.orangewall.surveys.resources.survey.model.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;




/**
 * Created by Arjan on 11/10/2017.
 */

@Path("surveys")
@Component
public class SurveyResource {

    @Autowired
    SurveyRepository surveyRepository;

    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    @Context
    HttpServletRequest requestContext;

    @GET
    @PermitAll
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getSurveyById(@PathParam("id") Integer id) {

        try {
            Survey surveyToReturn = surveyRepository.findOne(id);
            if (surveyToReturn == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            } else {
                return Response.ok(surveyToReturn).build();
            }
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

    }


    @GET
    @PermitAll
    @QueryParam("{name}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getSurveyByName(@QueryParam("name") String name) {

        try {
            List<Survey> surveyToReturn = surveyRepository.findByName(name);
            if (surveyToReturn == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            } else {
                return Response.ok(surveyToReturn).build();
            }
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

    }


    @POST
    @PermitAll
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response postSurvey(Survey survey) {

//    if(validateRequiredFieldsSurvey(survey)) {
        try {
            survey.setCreatedIP(requestContext.getRemoteAddr());
            survey.setCreatedbyId(1); //Implement logic to get UserId
            survey.setCreatedOn(LocalDateTime.now());
            survey.setActive(true);
            surveyRepository.save(survey);
        }
        catch(Exception ex) {

        }

        URI location = uriInfo.getAbsolutePathBuilder()
                .path("{id}")
                .resolveTemplate("id", survey.getId())
                .build();
        return Response.created(location).build();
//    }
//    else {
//        return Response.status(Response.Status.BAD_REQUEST).build();
//    }

 }

    @DELETE
    @PermitAll
    @Path("/{id}")
    public Response deleteSurvey(@PathParam("id") Integer id) {
        Response status = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        try {
            Survey surveyToDelete = surveyRepository.findOne(id);
            if (surveyToDelete == null) {
                status = Response.status(Response.Status.NOT_FOUND).build();
            } else {
                surveyRepository.delete(id);
                status = Response.status(Response.Status.ACCEPTED).build(); //Can also consider 204 (no content) or 200 (OK)
            }
        }
        catch(Exception ex){

            }

       return status;
    }

    @GET
    @PermitAll
    @Produces({MediaType.TEXT_PLAIN})
    @Path("/health")
    public String checkHeath() {
        return "SurveyResource service is healthy!";
    }


    private boolean validateRequiredFieldsSurvey(Survey surveyToValidate) {
        if(surveyToValidate.getName() == null || surveyToValidate.getName().isEmpty()) return false;
        else if(surveyToValidate.getContent() == null) return false;
        else return true;
    }

}
