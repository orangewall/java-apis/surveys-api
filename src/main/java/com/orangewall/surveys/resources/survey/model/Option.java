package com.orangewall.surveys.resources.survey.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;

/**
 * Created by Arjan on 11/18/2017.
 */
@XmlRootElement
//@Entity
public class Option {

    //@Id
    //@GeneratedValue
    private String id;
    private String text;


    // Must have no-argument constructor
    public Option() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
