package com.orangewall.surveys.resources.survey.model;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;

/**
 * Created by Arjan on 11/18/2017.
 */
@XmlRootElement
//@Entity
public class Question {

    //@Id
    //@GeneratedValue
    private String id;
    private String question;
    private Option[] options;
    private HashMap<String, Question> flow;



    // Must have no-argument constructor
    public Question() {

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Option[] getOptions() {
        return options;
    }

    public void setOptions(Option[] options) {
        this.options = options;
    }

    public HashMap<String, Question>  getFlow() {
        return flow;
    }

    public void setFlow(HashMap<String, Question>  flow) {
        this.flow = flow;
    }
}
