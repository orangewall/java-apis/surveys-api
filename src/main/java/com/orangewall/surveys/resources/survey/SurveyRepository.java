package com.orangewall.surveys.resources.survey;

import com.orangewall.surveys.resources.survey.model.Survey;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by Arjan on 11/26/2017.
 */
public interface SurveyRepository extends PagingAndSortingRepository<Survey, Integer>{
    List<Survey> findByName(String name);
}
