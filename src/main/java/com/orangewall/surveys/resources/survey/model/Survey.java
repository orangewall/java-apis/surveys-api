package com.orangewall.surveys.resources.survey.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orangewall.surveys.resources.survey.model.types.QuestionUserType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;

/**
 * Created by Arjan on 11/18/2017.
 */
@XmlRootElement
@Entity
@TypeDef(name = "QuestionUserType", typeClass = QuestionUserType.class)
@Table(name = "surveys")
public class Survey {

    @Id
    @GeneratedValue
    private Integer id;
    @Type(type = "QuestionUserType")
    private Question content;
    private boolean active;
    private String name;
    @JsonIgnore
    private Integer createdbyId;
    @JsonIgnore
    private Integer modifiedbyId;
    @JsonIgnore
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime createdOn;
    @JsonIgnore
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime modifiedOn;
    @JsonIgnore
    private String createdIP;
    @JsonIgnore
    private String modifiedIP;


    // Must have no-argument constructor
    public Survey() {

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Question  getContent() {
        return content;
    }

    public void setContent(Question content) {
        this.content = content;
    }

    public Integer getCreatedbyId() {
        return createdbyId;
    }

    public void setCreatedbyId(Integer createdbyId) {
        this.createdbyId = createdbyId;
    }

    public Integer getModifiedbyId() {
        return modifiedbyId;
    }

    public void setModifiedbyId(Integer modifiedbyId) {
        this.modifiedbyId = modifiedbyId;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDateTime getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(LocalDateTime modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getCreatedIP() {
        return createdIP;
    }

    public void setCreatedIP(String createdIP) {
        this.createdIP = createdIP;
    }

    public String getModifiedIP() {
        return modifiedIP;
    }

    public void setModifiedIP(String modifiedIP) {
        this.modifiedIP = modifiedIP;
    }
}
