package com.orangewall.surveys.resources.user;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    User getByUsername(String username);

}