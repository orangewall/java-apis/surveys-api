package com.orangewall.surveys.resources.callRecords;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.Id;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Created by Arjan on 3/05/2018.
 */

//@XmlRootElement
@Entity
@Table(name = "CallRecords")
public class CallRecords {

    @Id
    @GeneratedValue
    private Integer id;
    @NotNull
    private String awsAccountId;
    private String awsContactTraceRecordFormatVersion;
    private Double afterContactWorkDuration;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime afterContactWorkEndTimestamp;
    private Integer agentInteractionDuration;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime connectedToAgentTimestamp;
    private Integer customerHoldDuration;
    private Integer longestHoldDuration;
    private Integer numberOfHolds;
    private String userId;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime connectedToSystemTimestamp;
    private String contactId;
    private String systemEndpointAddress;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime disconnectTimestamp;
    private String initiationMethod;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime initiationTimestamp;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime lastUpdateTimestamp;
    private Double duration;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime enqueueTimestamp;
    private String queueName;
    private String recordingLocation;
    private String customerEndpointAddress;


    public CallRecords() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAwsAccountId() {
        return awsAccountId;
    }

    public void setAwsAccountId(String awsAccountId) {
        this.awsAccountId = awsAccountId;
    }

    public String getAwsContactTraceRecordFormatVersion() {
        return awsContactTraceRecordFormatVersion;
    }

    public void setAwsContactTraceRecordFormatVersion(String awsContactTraceRecordFormatVersion) {
        this.awsContactTraceRecordFormatVersion = awsContactTraceRecordFormatVersion;
    }

    public Double getAfterContactWorkDuration() {
        return afterContactWorkDuration;
    }

    public void setAfterContactWorkDuration(Double afterContactWorkDuration) {
        this.afterContactWorkDuration = afterContactWorkDuration;
    }

    public LocalDateTime getAfterContactWorkEndTimestamp() {
        return afterContactWorkEndTimestamp;
    }

    public void setAfterContactWorkEndTimestamp(LocalDateTime afterContactWorkEndTimestamp) {
        this.afterContactWorkEndTimestamp = afterContactWorkEndTimestamp;
    }

    public Integer getAgentInteractionDuration() {
        return agentInteractionDuration;
    }

    public void setAgentInteractionDuration(Integer agentInteractionDuration) {
        this.agentInteractionDuration = agentInteractionDuration;
    }

    public LocalDateTime getConnectedToAgentTimestamp() {
        return connectedToAgentTimestamp;
    }

    public void setConnectedToAgentTimestamp(LocalDateTime connectedToAgentTimestamp) {
        this.connectedToAgentTimestamp = connectedToAgentTimestamp;
    }

    public Integer getCustomerHoldDuration() {
        return customerHoldDuration;
    }

    public void setCustomerHoldDuration(Integer customerHoldDuration) {
        this.customerHoldDuration = customerHoldDuration;
    }

    public Integer getLongestHoldDuration() {
        return longestHoldDuration;
    }

    public void setLongestHoldDuration(Integer longestHoldDuration) {
        this.longestHoldDuration = longestHoldDuration;
    }

    public Integer getNumberOfHolds() {
        return numberOfHolds;
    }

    public void setNumberOfHolds(Integer numberOfHolds) {
        this.numberOfHolds = numberOfHolds;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public LocalDateTime getConnectedToSystemTimestamp() {
        return connectedToSystemTimestamp;
    }

    public void setConnectedToSystemTimestamp(LocalDateTime connectedToSystemTimestamp) {
        this.connectedToSystemTimestamp = connectedToSystemTimestamp;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getSystemEndpointAddress() {
        return systemEndpointAddress;
    }

    public void setSystemEndpointAddress(String systemEndpointAddress) {
        this.systemEndpointAddress = systemEndpointAddress;
    }

    public LocalDateTime getDisconnectTimestamp() {
        return disconnectTimestamp;
    }

    public void setDisconnectTimestamp(LocalDateTime disconnectTimestamp) {
        this.disconnectTimestamp = disconnectTimestamp;
    }

    public String getInitiationMethod() {
        return initiationMethod;
    }

    public void setInitiationMethod(String initiationMethod) {
        this.initiationMethod = initiationMethod;
    }

    public LocalDateTime getInitiationTimestamp() {
        return initiationTimestamp;
    }

    public void setInitiationTimestamp(LocalDateTime initiationTimestamp) {
        this.initiationTimestamp = initiationTimestamp;
    }

    public LocalDateTime getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(LocalDateTime lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public LocalDateTime getEnqueueTimestamp() {
        return enqueueTimestamp;
    }

    public void setEnqueueTimestamp(LocalDateTime enqueueTimestamp) {
        this.enqueueTimestamp = enqueueTimestamp;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getRecordingLocation() {
        return recordingLocation;
    }

    public void setRecordingLocation(String recordingLocation) {
        this.recordingLocation = recordingLocation;
    }

    public String getCustomerEndpointAddress() {
        return customerEndpointAddress;
    }

    public void setCustomerEndpointAddress(String customerEndpointAddress) {
        this.customerEndpointAddress = customerEndpointAddress;
    }
}
