package com.orangewall.surveys.resources.callRecords;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Arjan on 3/7/2018.
 */
@XmlRootElement
public class EmployeeEngagement {
    @Id
    @GeneratedValue
    private Integer id;
    private Double totalMinutes;
    private String userId; //agent
    private Long totalConversations;
    private Long uniqueConversations;
    private Long liveConversation;
    private Double averageTalkTime;
    private Double averageAfterCallDuration;


    public EmployeeEngagement(String userId, Long totalConversations) {
        this.userId = userId;
        this.totalConversations = totalConversations;
    }


    public EmployeeEngagement(Double totalMinutes, String userId, Long totalConversations, Long uniqueConversations, Long liveConversation, Double averageTalkTime, Double averageAfterCallDuration) {
        this.totalMinutes = totalMinutes;
        this.userId = userId;
        this.totalConversations = totalConversations;
        this.uniqueConversations = uniqueConversations;
        this.liveConversation = liveConversation;
        this.averageTalkTime = averageTalkTime;
        this.averageAfterCallDuration = averageAfterCallDuration;
    }


    public String getStaffId() {
        return userId;
    }

    public void setStaffId(String staffId) {
        this.userId = staffId;
    }

    public Long getTotalConversations() {
        return totalConversations;
    }

    public void setTotalConversations(Long totalConversations) {
        this.totalConversations = totalConversations;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getUniqueConversations() {
        return uniqueConversations;
    }

    public void setUniqueConversations(Long uniqueConversations) {
        this.uniqueConversations = uniqueConversations;
    }

    public Long getLiveConversation() {
        return liveConversation;
    }

    public void setLiveConversation(Long liveConversation) {
        this.liveConversation = liveConversation;
    }

    public Double getAverageTalkTime() {
        return averageTalkTime;
    }

    public void setAverageTalkTime(Double averageTalkTime) {
        this.averageTalkTime = averageTalkTime;
    }

    public Double getAverageAfterCallDuration() {
        return averageAfterCallDuration;
    }

    public void setAverageAfterCallDuration(Double averageAfterCallDuration) {
        this.averageAfterCallDuration = averageAfterCallDuration;
    }

//    public String getAwsAccountId() {
//        return awsAccountId;
//    }
//
//    public void setAwsAccountId(String awsAccountId) {
//        this.awsAccountId = awsAccountId;
//    }

    public Double getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(Double totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

//    public Integer getUniqueConversations() {
//        return uniqueConversations;
//    }
//
//    public void setUniqueConversations(Integer uniqueConversations) {
//        this.uniqueConversations = uniqueConversations;
//    }
//
//    public Integer getLiveConversation() {
//        return liveConversation;
//    }
//
//    public void setLiveConversation(Integer liveConversation) {
//        this.liveConversation = liveConversation;
//    }
//
//    public Number getAverageTalkTime() {
//        return averageTalkTime;
//    }
//
//    public void setAverageTalkTime(Number averageTalkTime) {
//        this.averageTalkTime = averageTalkTime;
//    }
//
//    public Number getAverageAfterCallDuration() {
//        return averageAfterCallDuration;
//    }
//
//    public void setAverageAfterCallDuration(Number averageAfterCallDuration) {
//        this.averageAfterCallDuration = averageAfterCallDuration;
//    }
//
//    public String getAwsAccountId() {
//        return awsAccountId;
//    }
//
//    public void setAwsAccountId(String awsAccountId) {
//        this.awsAccountId = awsAccountId;
//    }
//
//    public Integer getDuration() {
//        return duration;
//    }
//
//    public void setDuration(Integer duration) {
//        this.duration = duration;
//    }
//
//    public LocalDateTime getConnectedToSystemTimestamp() {
//        return connectedToSystemTimestamp;
//    }
//
//    public void setConnectedToSystemTimestamp(LocalDateTime connectedToSystemTimestamp) {
//        this.connectedToSystemTimestamp = connectedToSystemTimestamp;
//    }
}
