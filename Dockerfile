FROM 560453631936.dkr.ecr.us-east-1.amazonaws.com/common-images/java8-base:latest
LABEL maintainer="Marseld Dedgjonaj <m.dedgjonaj@gmail.com>"

# Make port 8100 available to the world outside this container
EXPOSE 8100

ENTRYPOINT ["java", "-jar", "/usr/share/myservice/myservice.jar"]

# Add Maven dependencies (not shaded into the artifact; Docker-cached)
#ADD target/lib           /usr/share/myservice/lib

# Add the service itself
ADD target/myservice.jar /usr/share/myservice/myservice.jar